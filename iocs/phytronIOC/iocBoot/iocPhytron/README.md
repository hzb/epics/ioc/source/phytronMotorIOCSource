## Generic Phytron IOC

To increase the number of channels you need to:

1. modify `motorPhytron.cmd` and add more channels
2. modify `motor.substitutions.phytron` and add more channels. Optionally change the name. Be sure to use the correct ADDR
3. modify `settings.req`

The `st.cmd.phytron` file will pull in these files and build the ioc accordingly